// pages/home/home.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo:wx.getStorageSync('userInfo'),
    openid:wx.getStorageSync('openid'),
    index: 0,
    gonggao:'',
    tongzhi:'',
    version:'',
    current: 0,  //当前所在页面的 index
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 5000, //自动切换时间间隔
    duration: 800, //滑动动画时长
    circular: true, //是否采用衔接滑动
    imgUrls: [
      'https://www.qdxqjy.cn/yuanhang_vxMiniapps/lunbotu/1.jpg',
      'https://www.qdxqjy.cn/yuanhang_vxMiniapps/lunbotu/2.jpg',
      'https://www.qdxqjy.cn/yuanhang_vxMiniapps/lunbotu/3.jpg'
    ],
    links: [
      '/pages/second/register',
      '/pages/second/register',
      '/pages/second/register'
    ]
  },
  getUserInfo(e){
    //第二步 调用云函数
    wx.cloud.callFunction({
      name:'login',
      success:(res)=>{
        this.setData({
          openid:res.result.openid,
          userInfo:e.detail.userInfo
        })
        wx.setStorageSync('openid', res.result.openid)
        wx.setStorageSync('userInfo', e.detail.userInfo)
        if(res.result.isUser==0){//登录时 服务端已经做了查询数据库 时候有当前的openid
          // 注册
          wx.cloud.callFunction({
            name:'register',
            data:{
              nickName:e.detail.userInfo.nickName,
              avatarUrl:e.detail.userInfo.avatarUrl,
              sex:e.detail.userInfo.sex
            }
          })
        }
      }
    })
  },
  
  calling: function () {
    wx.makePhoneCall({
      phoneNumber: '13026597102',
    })
  },
  //轮播图的切换事件
  swiperChange: function (e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  //点击指示点切换
  chuangEvent: function (e) {
    this.setData({
      swiperCurrent: e.currentTarget.id
    })
  },
  //点击图片触发事件
  swipclick: function (e) {
    console.log(this.data.swiperCurrent);
    wx.switchTab({
      url: this.data.links[this.data.swiperCurrent]
    })
  },

  setUserInfo(){//设置兼职
    wx.navigateTo({
      url: '../editUserInfo/editUserInfo',
    })
  },
  gotoJoin(){//查看参与的打卡
    wx.navigateTo({
      url: '../play/play',
    })
  },
  gotoList(){//查看创建的打卡
    wx.navigateTo({
      url: '../clock_list/clock_list',
    })
  },
  gotoCreate(){//创建打卡
    wx.navigateTo({
      url: '../create/create',
    })
  },
  gotoPlayclockList(){//管理打卡
    wx.navigateTo({
      url: '../playclock_list/playclock_list',
    })
  }, 
  gotoDangqianXuanze(){
    wx.navigateTo({
      url: '../playList/playList',
    })
  },
  gotoChengfa(){
    wx.navigateTo({
      url: '../punish/punish',
    })
  },
  gotoReward(){
    wx.previewImage({
      current: 'https://www.qdxqjy.cn/yuanhang_vxMiniapps/dashang/dashang.png', // 当前显示图片的http链接
      urls: ['https://www.qdxqjy.cn/yuanhang_vxMiniapps/dashang/dashang.png']
    })
  },
  gotoLianxiwomen() {
    wx.previewImage({
      current: 'https://www.qdxqjy.cn/yuanhang_vxMiniapps/lianxiwomen/lianxi.jpg', // 当前显示图片的http链接
      urls: ['https://www.qdxqjy.cn/yuanhang_vxMiniapps/lianxiwomen/lianxi.jpg']
    })
  },

  direction(){
    wx.navigateTo({
      url: '../image/image',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.cloud.callFunction({
      name: 'yuanhang',
      success: (res) => {
        this.setData({
          gonggao: res.result.data[0].gonggao,
          tongzhi: res.result.data[0].tongzhi,
          version: res.result.data[0].version
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let clearInfo=wx.getStorageSync('clearClassInfo')
    if(clearInfo==true){
      wx.removeStorageSync('classInfo')
      wx.setStorageSync('clearClassInfo', false)
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.removeStorageSync('classInfo')
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '远航打卡-VX:roadisvx',
      path: '/pages/home/home', //用户点开后的默认页面，我默认为首页
      imageUrl: "/assets/loa.png", //自定义图片的地址
      success(res) {
        console.log('发送成功！')
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  onShareTimeline: function (res) {
    return {
      title: '远航打卡-VX:roadisvx'
    }
  }
})