// pages/create/create.js
const {formatDate}=require('../../utils/util.js') //获取日期转换函数
Page({
  sbfl:true,
  data: {//页面的初始数据
    openid:wx.getStorageSync('openid'),
    gonggao:'',
    tongzhi:'',
    title:'',
    startDate: formatDate(new Date),
    endDate: formatDate(new Date),
    startTime:'08:00',
    endTime:'18:00',
    targetMap:{//选择打卡地点
      latitude:0,//纬度
      longitude:0,//经度
      name:'',//地名
      range:'1000'//距离
    },
    classNo:'',
    writeChoice:[//打卡者填写项
      {type:'userName',notes:'姓名',state:true},
      {type:'userPhone',notes:'手机号',state:false},
      {type:'userId',notes:'身份证号',state:false}],
    isEveryWrite:{//是否必填
      list:['每次必填','仅第一次'],
      index:0
    }
  },
  mgcCheck(val,success){// 敏感词校验
    val=val.trim()
    if(val!=""&&val!=null){
      wx.cloud.callFunction({
        name: 'msgSC',
        data: { text: val }//需要检测的内容
      }).then((res) => {
        if (res.result.code == "200") {//检测通过
          success()//回调函数
        } else {//执行不通过
          wx.showToast({
            title: '包含敏感字哦。',
            icon: 'none',
            duration: 3000
          })
        }
      })
    }
  },
  setTitle(e){//设置打卡标题
    let val=e.detail.value
    this.mgcCheck(val,()=>{//调用敏感词校验函数 
      this.setData({
        title:val
      })
    })
  },
  changeDate: function(e) {//选择时间
    let timeType=e.target.dataset.type
    console.log(e.detail.value);
    if(timeType=='startDate'||timeType=='endDate'){
      let currentTimeNum=new Date(...e.detail.value.split('-')).getTime()
      let startDateNum=new Date(...this.data.startDate.split('-')).getTime()
      let endDateNum=new Date(...this.data.endDate.split('-')).getTime()
      if((timeType=='startDate'&&currentTimeNum<=endDateNum) || (timeType=='endDate'&&currentTimeNum>=startDateNum)){
        this.setData({
          [timeType]: e.detail.value
        })
      }else{
        wx.showToast({
          title: timeType=='startDate'?'开始时间不能大于结束时间':'结束时间不能小于开始时间',
          icon:"none"
        })
      }
    }else{
      this.setData({
        [timeType]: e.detail.value
      })
    }
  },
  setMap(){// 设置位置 获取经纬度
    console.log('setmap clicking');
    wx.chooseLocation({
      success:(res)=>{
        console.log(res);
        this.setData({
          ['targetMap.latitude']:res.latitude,
          ['targetMap.longitude']:res.longitude,
          ['targetMap.name']:res.name,
        })
      },
      fail:(res)=>{
        // 判断用户是否授权
        wx.getSetting({
          success: (res) => {
            var statu = res.authSetting;
            if (!statu['scope.userLocation']) { //没授权
              wx.showModal({
                title: '是否授权当前位置',
                content: '需要获取您的地理位置，请确认授权',
                confirmColor: '#f16765',
                success: res => {
                  if (res.confirm) {
                    wx.openSetting({
                      success: data => {
                        if (data.authSetting["scope.userLocation"]) {
                          this.setMap()
                        }
                      }
                    })
                  }
                }
              })
            } 
          }
        })
      }
    })
  },
  setClassNo(e){//设置兼职
    this.setData({
      classNo:e.detail.value
    })
  },
  setrange(e) {// 设置距离
    this.setData({
      ['targetMap.range']: e.detail.value
    })
  },
  changeCurrent(e){// 更改打卡者必填信息
    let id=e.currentTarget.dataset.id
    this.data.writeChoice.forEach((item,index,arr)=>{
      if(index == id) {
        let oSelected = "writeChoice[" + index + "].state"//这里需要将设置的属性用字符串进行拼接
        console.log(oSelected);
        this.setData({
          [oSelected]: !this.data.writeChoice[index].state
        })
      }
    })
  },
  changeIsEveryWrite(e){// 是否每次填写
    let id=e.detail.value
    let oSelected='isEveryWrite.index'
    this.setData({
      [oSelected]:id
    })
  },
  
  createClockEvent(){// 提交所有打卡信息
    let that=this
    if(this.data.title!=''&&this.data.classNo!=''&&this.data.targetMap.latitude!=0&&that.sbfl){
      that.sbfl=false
      wx.cloud.callFunction({
        name:'create_clock',
        data:this.data,//提交整个页面的data数据
        success(res){
          if(res.result.errMsg=='collection.add:ok'){
            wx.showToast({
              title: '创建成功',
              success(){
                setTimeout(()=>{
                  wx.navigateBack()
                },2000)
              }
            })
          }
        }
      })
    }else{
      wx.showToast({
        title: '打卡标题或兼职名或地址未填写',
        icon:"none"
      })
    }
  },
  goToHome(){
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      openid:wx.getStorageSync('openid')
    }),
    wx.cloud.callFunction({
      name: 'yuanhang',
      success: (res) => {
        this.setData({
          gonggao: res.result.data[0].gonggao,
          tongzhi: res.result.data[0].tongzhi
        })
      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '远航打卡-VX:roadisvx',
      path: '/pages/home/home', //用户点开后的默认页面，我默认为首页
      imageUrl: "/assets/loa.png", //自定义图片的地址
      success(res) {
        console.log('发送成功！')
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  onShareTimeline: function (res) {
    return {
      title: '远航打卡-VX:roadisvx'
    }
  }
})