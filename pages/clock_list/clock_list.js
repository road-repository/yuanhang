// pages/clock_list/clock_list.js
Page({/*** 页面的初始数据*/
  data: {
    userInfo:null,
    clock_list:[],
  },
  onLoad: function (options) {/*** 生命周期函数--监听页面加载*/
    let userInfo=wx.getStorageSync('userInfo')
    let openid=wx.getStorageSync('openid')
    if(!openid){
      wx.showToast({
        title: '请先登录',
        icon:"loading",
        success(){
          setTimeout(()=>{
            wx.navigateBack()
          },2000)
        }
      })
    }
    let classInfo=wx.getStorageSync('classInfo')
    wx.showLoading({
      title: '正在加载',
    })
    wx.cloud.callFunction({
      name:'get_clock_list',
      data:{
        openid,//创建的打卡列表 只用传自己的openid
        // classNo:classNo[classIndex]
      },
      success:(res)=>{
        console.log(res.result.clock_list);
        this.setData({
          userInfo:userInfo,
          clock_list:res.result.clock_list.sort(function(a,b){
            return a.classNo-b.classNo
          })
        })
        wx.hideLoading()
      },
      fail(err){
        wx.hideLoading()
        console.log(err);
      }
    })
  },

  setSadianClock: function (e) {
    let deleteId = e.currentTarget.dataset.smile
    var that = this;
    wx.showModal({
      title: '请确认',
      content: `您确认此条打卡吗?`,
      confirmText: '确认',
      success(res) {
        if (res.confirm) {
          wx.cloud.callFunction({//获取打卡列表 如果列表已经存在 打卡者 且 是同一个兼职 则不add 而是 update
            name: "delete_clock",
            data: { deleteId },
            success: (res) => {
              wx.setStorageSync('clearClassInfo',true)
              if (res.result.removeClockState.stats.removed === 1) {
                that.onLoad()
                wx.hideLoading()
                wx.showToast({
                  title: '删除记录成功！',
                  icon: 'success',
                  duration: 2000
                })
              } else {
                wx.showToast({
                  title: '删除记录失败……',
                  icon: 'loading',
                  duration: 3000
                })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  setDeleteClock: function () {
    this.setData({
      btnBgBackground: "red",
      btnBgValue: "删除"
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '远航打卡-VX:roadisvx',
      path: '/pages/home/home', //用户点开后的默认页面，我默认为首页
      imageUrl: "/assets/loa.png", //自定义图片的地址
      success(res) {
        console.log('发送成功！')
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  onShareTimeline: function (res) {
    return {
      title: '远航打卡-VX:roadisvx'
    }
  }
})