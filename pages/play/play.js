// pages/play/play.js
const {formatDate, formatTime,formatDateTime,geoDistance}=require('../../utils/util.js') //获取日期转换函数 及 距离测算函数
Page({
  flap:true,
  data: {/*** 页面的初始数据*/
    resetInputValue:'',
    clock_info:null,
    markers:null,
    userInfo:null,//到时候要展示打卡记录  的昵称和头像
    clock_logs:null
  },
  onLoad: function (options) {/*** 生命周期函数--监听页面加载 */
    let classInfo=wx.getStorageSync('classInfo')
    let classNo=classInfo.classNo
    let classIndex=classInfo.index
    if(classIndex==0 || classIndex==undefined){
      wx.showToast({
        title: '请先登录或在编辑资料选择兼职!',
        icon:"none",
        success(){
          setTimeout(()=>{
            wx.navigateBack()
          },2000)
        }
      })
    }else{
      this.setData({
        userInfo:wx.getStorageSync('userInfo')
      })
      this.get_current_clock(classNo[classIndex])// 查询 当前兼职的打卡信息 并更新地图
      this.get_logs(classNo[classIndex])//获取打卡记录
    }
  },
  get_current_clock(classNo){// 查询 当前班级的打卡信息 并更新地图
    wx.cloud.callFunction({
      name:'get_clock_list',
      data:{ classNo },//参与的打卡 只用传自己的班级
      success:(res)=>{
        this.setData({
          clock_info:res.result.clock_list[0],
          markers:[{
            iconPath: "../../assets/icon_mark.png",
            id: 0,
              latitude:res.result.clock_list[0].targetMap.latitude,
              longitude:res.result.clock_list[0].targetMap.longitude,
              range: res.result.clock_list[0].targetMap.range,
              width: 30,
              height: 30
          }]
        })
      },
      fail(err){
        wx.showToast({
          title: '请先在编辑资料选择兼职!',
          icon: "none",
          success() {
            setTimeout(() => {
              wx.navigateBack()
            }, 2000)
          }
        })
      }
    })
  },
  setSadianClock: function (e) {
    let deleteId = e.currentTarget.dataset.smile
    wx.showModal({
      title: '请确认',
      content: `确定删除此条打卡记录?`,
      confirmText: '确认',
      success(res) {
        if (res.confirm) {
          wx.cloud.callFunction({//获取打卡列表 如果列表已经存在 打卡者 且 是同一个兼职 则不add 而是 update
            name: "clock_logs_delete",
            data: { deleteId },
            success: (res) => {
              if (res.result.stats.removed === 1) {
                wx.showToast({
                  title: '删除记录成功！',
                  icon: 'success',
                  duration: 2000
                })
                setTimeout(() => {
                  wx.navigateBack()
                }, 2000)
              } else {
                wx.showToast({
                  title: '删除失败……',
                  icon: 'loading',
                  duration: 3000
                })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  setDeleteClock: function () {
    this.setData({
      btnBgBackground: "red",
      btnBgValue: "删除"
    })
  },

  mgcCheck(val,success){// 敏感词校验
    val = val.trim()
    if (val != "" && val != null) {
      wx.cloud.callFunction({
        name: 'msgSC',
        data: {text:val}//需要检测的内容
      }).then((res) => {
        if (res.result.code == "200") {//检测通过
          success()//回调函数
        } else {//执行不通过
          wx.showToast({
            title: '包含敏感字哦。',
            icon: 'none',
            duration: 3000
          })
        }
      })
    }
  },

  get_logs(classNo){//获取打卡记录
    wx.cloud.callFunction({//获取打卡列表 如果列表已经存在 打卡者 且 是同一个兼职 则不add 而是 update
      name:"clock_logs_get",
      data:{classNo},
      success:(res)=>{
        this.setData({
          clock_logs:res.result.data[0]
        })
      }
    })
  },
  submitFunction(e){
    let that=this
    let curDate=formatDateTime(new Date());
    let user=e.detail.value
    if(that.flap){
      wx.showModal({
        title: '请确认打卡信息',
        // content: '请确认待整改项已整改完毕！',
        content: `${user.userName==undefined?"":"姓名:"+user.userName}\n${user.userPhone==undefined?"":"手机号:"+user.userPhone}\n${user.userId==undefined?"":"身份证:"+user.userId}\n${"时间:"+curDate}`,
        // 开发者工具上没有换行，真机调试时会有的
        confirmText: '确认',
        success(res) {
          if (res.confirm) {
            that.flap=false
            console.log('用户点击确定')
            that.play_clock(user)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },
  
  play_clock(inputuser){//点击打卡按钮时 提交打卡信息
    let clock_info = this.data.clock_info
    let classNo = clock_info.classNo
    let user=inputuser
    let startDate = this.data.clock_info.startDate.split("-").join('')
    let startTime = this.data.clock_info.startTime.split(":").join('')
    let endDate = this.data.clock_info.endDate.split("-").join('')
    let endTime = this.data.clock_info.endTime.split(":").join('')
    let date = formatDate(new Date)
    let time = formatTime(new Date)
    let bdate = date.split("-").join('')
    let btime = time.split(":").join('')
    let targetMap = this.data.clock_info.targetMap//打卡目标 经纬度
  
    //暂存创建打卡者需要哪些信息
    let notes=[];
  
    for (var i = 0; i < clock_info.writeChoice.length;i++){
      if (clock_info.writeChoice[i].state == true){
        notes[notes.length] = clock_info.writeChoice[i].type
      }
    }

    //校验姓名是否合法
    let nameCheck = /^[\u4e00-\u9fa5]{2,5}$/
    //检验手机号是否合法
    let phoneCheck = /^1[0-9]{10}$/
    //校验身份证号码是否合法
    let idCheck = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
    console.log("获取的"+user.userName+user.userPhone+user.userId)
    let userNameCheck=notes.indexOf("userName") == -1 ? true : nameCheck.test(user.userName) 
    let userPhoneCheck = notes.indexOf("userPhone") == -1 ? true :phoneCheck.test(user.userPhone)
    let userIdCheck = notes.indexOf("userId") == -1 ? true : idCheck.test(user.userId)
    if (userNameCheck&& userPhoneCheck && userIdCheck){
      wx.getLocation({//获取当前位置
        type: "gcj02",
        isHighAccuracy: true,//开启高精度定位
        success: (res) => {
          let currentMap = { latitude: res.latitude, longitude: res.longitude }//用户打卡时经纬度
          let sss = geoDistance(targetMap, currentMap)//计算打卡距离
          let curjuli = targetMap.range / 1000
          //大于设置的米数 则提示
          console.log(sss+"cur="+curjuli)
          if (sss >= curjuli || user == undefined || bdate < startDate || btime < startTime || bdate > endDate || btime > endTime) {
            wx.showToast({ title: '打卡失败!距离大于' + targetMap.range + '米，或请查看资料和合法打卡时间!', icon: "none" })
          } else {//小于设定的距离才 进行查询 打卡记录 然后新增或修改打卡记录
            console.log("用户"+this.data.userInfo.nickName)
            this.play_clock_fn(classNo, user, date, time, targetMap, currentMap, this.data.userInfo,clock_info._id)
          }
        },
        fail:(res)=>{
          // 判断用户是否授权
          wx.getSetting({
            success: (res) => {
              var statu = res.authSetting;
              if (!statu['scope.userLocation']) { //没授权
                wx.showModal({
                  title: '是否授权当前位置',
                  content: '需要获取您的地理位置，请确认授权',
                  confirmColor: '#f16765',
                  success: res => {
                    if (res.confirm) {
                      wx.openSetting({
                        success: data => {
                          if (data.authSetting["scope.userLocation"]) {
                            this.play_clock()
                          }
                        }
                      })
                    }
                  }
                })
              }
            }
          })
        }
      })
    }else{
      wx.showToast({
        title: '请正确填写个人信息!',
        icon: 'none'
      })
      this.flap=true
    }
  
  },
  play_clock_fn(classNo,user,date,time,targetMap,currentMap,userInfo,id){
    wx.cloud.callFunction({//获取打卡列表 如果列表已经存在 打卡者 且 是同一个班级 则不add 而是 update
      name:"clock_logs_get",
      data:{classNo},
      success:(res)=>{
        if(res.result.data.length<=0){
          this.add_logs(classNo, user, date, time, targetMap, currentMap, userInfo, id)//新增打卡记录
        }else{
          //  每天只能打一次卡 ：如果打卡记录中的日期不包含 今天的日期 才进行新增(更新)日期
          let isEveryDayOnce =res.result.data[0].logsInfo.every(item=>{
            return item.date!==date
          })
          if(isEveryDayOnce==true){
            this.update_logs(classNo, user, date, time, userInfo)//更新打卡记录
          }
          wx.showToast({
            title: isEveryDayOnce?"打卡成功":'今天你已经打过卡了',
            icon:'none'
          })
          this.setData({
            resetInputValue: ''
          })
          this.flap=true
        }
      }
    })
  },
  add_logs(classNo,user,date,time,targetMap,currentMap,userInfo,clockId){//新增打卡记录
    wx.cloud.callFunction({
      name:'clock_logs_add',
      data: { classNo, user, date, time, targetMap, currentMap, userInfo, clockId},
      success:(res)=>{
        this.get_logs(classNo)//打卡后 获取并更新打卡记录
        wx.showToast({
          title: '打卡成功',
          icon:"none"
        })
        this.setData({
          resetInputValue: ''
        })
        this.flap=true
      }
    })
  },
  update_logs(classNo, user, date, time, userInfo){//更新打卡记录
    wx.cloud.callFunction({//更新打卡记录 为每个人的打卡记录 新增每天的时间 进行打卡 即可
      name:"clock_logs_update",
      data: { classNo, user, date, time, userInfo},
      success:(res)=>{
        console.log(res);
        this.get_logs(classNo)//打卡后 获取并更新打卡记录
      },
      fail(err){
        console.log(err);
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '远航打卡-VX:roadisvx',
      path: '/pages/home/home', //用户点开后的默认页面，我默认为首页
      imageUrl: "/assets/loa.png", //自定义图片的地址
      success(res) {
        console.log('发送成功！')
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  onShareTimeline: function (res) {
    return {
      title: '远航打卡-VX:roadisvx'
    }
  }
})